import pandas as pd
import numpy as np
import json
import csv

from collections import defaultdict
from functools import reduce

from fca.api_models import Context

from costants import *
from order_esf import Order
from standardization import *


def generate_csv(
    maximum_rows: int = 100000,
    min_support: float = 0.5,
    min_confidence: float = 0.5,
    csv_file: str = "orders_ctx.csv",
    rules_file: str = "rules.csv",
):
    context = read_order_csv(maximum_rows)
    context.to_csv(csv_file)
    with open(rules_file, "w") as f:
        writer = csv.writer(f)
        writer.writerow(["support", "confidence", "base", "add"])
        for assoc_rule in context.get_association_rules(
            min_support=min_support, min_confidence=min_confidence
        ):
            support = assoc_rule.support
            for rule in assoc_rule.ordered_statistics:
                row = [
                    support,
                    rule.confidence,
                    f'{{{", ".join(x for x in rule.items_base)}}}',
                    f'{{{", ".join(x for x in rule.items_add)}}}',
                ]
                writer.writerow(row)


def generate_csv_by_resort(
    resort: int = 101,
    from_idx: int = 0,
    maximum_rows: int = 100000,
    min_support: float = 0.5,
    min_confidence: float = 0.5,
    csv_file: str = "orders_{}_ctx.csv",
    rules_file: str = "rules_{}.csv",
):
    context = read_order_csv(
        from_idx=from_idx,
        to_idx=from_idx + maximum_rows,
        orders_file=f"./orders/by_resort/orders_by_resort_{resort}.csv",
        orders_esf_file=f"./orders/esf_by_resort/orders_esf_by_resort_{resort}.csv",
        orders_custom_esf_file=f"./orders/custom_esf_by_resort/orders_custom_esf_by_resort_{resort}.csv",
    )
    #  assoc_rules = context.get_association_rules()
    context.to_csv(csv_file.format(resort))
    with open(rules_file.format(resort), "w") as f:
        writer = csv.writer(f)
        writer.writerow(["support", "confidence", "base", "add"])
        for assoc_rule in context.get_association_rules(
            min_support=min_support, min_confidence=min_confidence
        ):
            support = assoc_rule.support
            for rule in assoc_rule.ordered_statistics:
                row = [
                    support,
                    rule.confidence,
                    f'{{{", ".join(x for x in rule.items_base)}}}',
                    f'{{{", ".join(x for x in rule.items_add)}}}',
                ]
                writer.writerow(row)

    return {
        "context_filename": csv_file.format(resort),
        "rules_filename": rules_file.format(resort),
    }


def iterate_df(df, from_idx, to_idx, fn):
    for i, row in df.iterrows():
        if i < from_idx:
            continue
        if i >= to_idx:
            break

        fn(i, row)


def process_simple_orders(df_orders, from_idx, to_idx, orders):
    def fn(_, row):
        order = process_order(row)
        orders[order.id].append(order)

    iterate_df(df_orders, from_idx, to_idx, fn)


def process_esf_orders(df_orders_esf, from_idx, to_idx, orders):
    def fn(_, row):
        res = process_esf_order(row)
        order = Order(id=row.order_id, **res)
        orders[order.id].append(order)

    iterate_df(df_orders_esf, from_idx, to_idx, fn)


def process_esf_custom_orders(df_orders_custom_esf, from_idx, to_idx, orders):
    def fn(_, row):
        res = process_custom_esf_order(row)
        order = Order(id=row.order_id, **res)
        orders[order.id].append(order)

    iterate_df(df_orders_custom_esf, from_idx, to_idx, fn)


def create_context(orders):
    kinds = {
        f"kind={order.kind.lower()}"
        for order_id in orders
        for order in orders[order_id]
        if order.kind is not None
    }
    activities = {
        f"activity={order.activity.lower()}"
        for order_id in orders
        for order in orders[order_id]
        if order.activity is not None
    }
    levels = {
        f"level={order.level.lower()}"
        for order_id in orders
        for order in orders[order_id]
        if order.level is not None
    }
    # durations = {
    #     f'duration={sum([int(order.duration) for order in orders[order_id] if order.duration is not None])}' for order_id in orders}
    # nb_of_peoples = {
    #     f'nbPeople={sum([int(order.nb_people) for order in orders[order_id] if order.nb_people is not None])}' for order_id in orders}
    languages = {
        f"lang={str(order.lang)[5:]}"
        for order_id in orders
        for order in orders[order_id]
        if order.lang is not None
    }
    attributes = [
        "price_low",
        "price_mid",
        "price_high",
        "lift",
        "assu",
        "ski_rental",
        "duration<121",
        "121<=duration<300",
        "300<=duration",
        "nbPeople<2",
        "2<=nbPeople<4",
        "4<=nbPeople",
        "standard_product",
        "generic_line",
        "hotel",
        "ffs",
        "transfer",
        "stay_tax",
    ]
    attributes = [
        attr
        for lst in [attributes, kinds, activities, levels, languages]
        for attr in lst
    ]
    attr_idx = dict()
    for i, attr in enumerate(attributes):
        attr_idx[attr] = i

    context = Context([str(order_id) for order_id in orders], attributes, [])
    for order_id in orders:
        context.I.append([0 for _ in range(len(attributes))])
        total_nb_people = 0
        total_price = 0
        total_duration = 0
        for order in orders[order_id]:
            if order.duration is not None:
                if isinstance(order.duration, str):
                    order.duration = int(order.duration)
                total_duration += order.duration
                # context.I[-1][attr_idx['60<duraton']] = int(60 < order.duration)
            if order.nb_people is not None:
                if isinstance(order.nb_people, str):
                    order.nb_people = int(order.nb_people)
                total_nb_people += order.nb_people

            if order.price is not None:
                if isinstance(order.price, str):
                    order.price = float(order.price)
                total_price += order.price

            if order.kind:
                context.I[-1][attr_idx[f"kind={order.kind.lower()}"]] = 1
            if order.activity:
                context.I[-1][attr_idx[f"activity={order.activity.lower()}"]] = 1
            if order.level:
                context.I[-1][attr_idx[f"level={order.level.lower()}"]] = 1
            if order.assu:
                context.I[-1][attr_idx["assu"]] = 1
            if order.lift:
                context.I[-1][attr_idx["lift"]] = 1
            if order.lang is not None:
                context.I[-1][attr_idx[f"lang={str(order.lang)[5:]}"]] = 1
            if order.ski_rental:
                context.I[-1][attr_idx["ski_rental"]] = 1
            if order.standard_product:
                context.I[-1][attr_idx["standard_product"]] = 1
            if order.generic_line:
                context.I[-1][attr_idx["generic_line"]] = 1
            if order.hotel:
                context.I[-1][attr_idx["hotel"]] = 1
            if order.ffs:
                context.I[-1][attr_idx["ffs"]] = 1
            if order.transfer:
                context.I[-1][attr_idx["transfer"]] = 1
            if order.stay_tax:
                context.I[-1][attr_idx["stay_tax"]] = 1

        context.I[-1][attr_idx["duration<121"]] = int(bool(total_duration < 121))
        context.I[-1][attr_idx["121<=duration<300"]] = int(
            bool(121 <= total_duration < 300)
        )
        context.I[-1][attr_idx["300<=duration"]] = int(bool(300 <= total_duration))
        context.I[-1][attr_idx["nbPeople<2"]] = int(bool(total_nb_people < 2))
        context.I[-1][attr_idx["2<=nbPeople<4"]] = int(bool(2 <= total_nb_people < 4))
        context.I[-1][attr_idx["4<=nbPeople"]] = int(bool(4 <= total_nb_people))
        context.I[-1][attr_idx["price_low"]] = int(total_price < 300)
        context.I[-1][attr_idx["price_mid"]] = int(300 <= total_price < 800)
        context.I[-1][attr_idx["price_high"]] = int(800 <= total_price)
    return context


def read_order_csv(
    from_idx: int = 0,
    to_idx: int = None,
    orders_file: str = "./orders/orders-csv.csv",
    orders_esf_file: str = "./orders/orders-esf-csv.csv",
    orders_custom_esf_file: str = "./orders/orders-esf-custom-csv.csv",
) -> Context:
    if to_idx is None:
        to_idx = from_idx
        from_idx = 0

    # by default it considers the csv to have a header
    df_orders = pd.read_csv(orders_file)
    df_orders = df_orders[df_orders["order_id"].notnull()]
    df_orders_esf = pd.read_csv(orders_esf_file)
    df_orders_custom_esf = pd.read_csv(orders_custom_esf_file)

    orders = defaultdict(list)
    to_idx = to_idx if to_idx is not None else df_orders.size
    process_simple_orders(df_orders, from_idx, to_idx, orders)
    process_esf_orders(df_orders_esf, from_idx, to_idx, orders)
    process_esf_custom_orders(df_orders_custom_esf, from_idx, to_idx, orders)
    context = create_context(orders)
    return context


def read_order_csv_by_resort(
    from_idx: int = 0, to_idx: int = None, resort: int = 101
) -> Context:
    if to_idx is None:
        to_idx = from_idx
        from_idx = 0

    # by default it considers the csv to have a header
    df_orders = pd.read_csv(f"orders/by_resort/orders_by_resort_{resort}.csv")
    df_orders = df_orders[df_orders["order_id"].notnull()]
    df_orders_esf = pd.read_csv(
        f"orders/esf_by_resort/orders_esf_by_resort_{resort}.csv"
    )
    df_orders_custom_esf = pd.read_csv(
        f"orders/custom_esf_by_resort/orders_custom_esf_by_resort_{resort}.csv"
    )

    orders = dict()
    to_idx = to_idx if to_idx is not None else df_orders.size
    process_simple_orders(df_orders, from_idx, to_idx, orders)
    process_esf_orders(df_orders_esf, from_idx, to_idx, orders)
    process_esf_custom_orders(df_orders_custom_esf, from_idx, to_idx, orders)
    context = create_context(orders)
    return context


def process_order(row):
    lang = Lang.FR if row.lang == "fr" else Lang.EN
    lift = row.lift == 1
    assu = row.assu == 1
    ski_rental = row.ski_rental == 1
    standard_product = row.standard_product == 1
    generic_line = row.generic_line == 1
    hotel = row.hotel == 1
    ffs = row.ffs == 1
    transfer = row.transfer == 1
    stay_tax = row.stay_tax == 1

    kind, level, activity, duration, nb_people, price = (
        None,
        None,
        None,
        None,
        None,
        None,
    )

    return Order(
        id=row.order_id,
        assu=assu,
        lang=lang,
        lift=lift,
        ski_rental=ski_rental,
        standard_product=standard_product,
        generic_line=generic_line,
        hotel=hotel,
        ffs=ffs,
        transfer=transfer,
        stay_tax=stay_tax,
        activity=activity,
        kind=kind,
        level=level,
        duration=duration,
        nb_people=nb_people,
        price=price,
    )


def process_esf_order(row):
    order_id = row.order_id
    price = process_price(row.price)
    if pd.isna(row.inscription):
        res = process_inscriptions(row.inscriptions)
    else:
        res = process_inscription(row)

    return {
        "kind": kind_codes(res["kind"]) if res["kind"] is not None else None,
        "level": str(res["level"]),
        "activity": res["activity"],
    }


def process_custom_esf_order(row):
    order_id = row.order_id
    price = process_price(row.price)
    res = process_inscription_custom(row.inscription)
    return {
        "kind": kind_codes(res["kind"]) if not res["kind"] else None,
        "level": str(res["level"]) if res["level"] else None,
        "activity": custom_esf_discipline(res["activity_label"], res["activity_code"]),
        "duration": res["duration"],
        "nb_people": res["nb_people"],
        "price": price,
    }


def process_price(price):
    # if price < 200:
    #     return 'PriceLow'
    # if price < 400:
    #     return 'PriceMed'
    return price


def process_inscriptions(inscriptions):
    json_inscriptions = json.loads(inscriptions)
    kind, level, activity = None, None, None
    if len(json_inscriptions) > 0:
        first_product = json_inscriptions[0]
        kind = maybe(
            first_product,
            "codes",
            lambda codes: maybe(
                codes,
                "kinds",
                lambda kinds: kinds[0].lower() if isinstance(kinds[0], str) else None,
            ),
        )
        level = maybe(
            first_product,
            "codes",
            lambda codes: maybe(
                codes,
                "levels",
                lambda levels: (
                    levels[0].lower() if isinstance(levels[0], str) else None
                ),
            ),
        )
        activity = maybe(
            first_product,
            "codes",
            lambda codes: maybe(
                codes,
                "activities",
                lambda activities: (
                    activities[0].lower() if isinstance(activities[0], str) else None
                ),
            ),
        )
    return {
        "kind": kind_codes(kind) if kind is not None else None,
        "level": level,
        "activity": custom_esf_discipline(activity, activity),
    }


def process_inscription(order_esf):
    return {
        "kind": order_esf.kind_code,
        "level": order_esf.level_code,
        "activity": custom_esf_discipline(None, order_esf.activity_code),
    }


def process_inscription_custom(inscription_json):
    inscription = json.loads(inscription_json)
    return {
        "kind": "cp",
        "level": maybe(
            inscription, "pax", lambda pax: pax[0].get("level") if pax else None
        ),
        "activity_code": maybe(
            inscription, "activity", lambda activity: activity.get("code")
        ),
        "activity_label": maybe(
            inscription, "activity", lambda activity: activity.get("label")
        ),
        "duration": inscription.get("duration"),
        "nb_people": inscription.get("nbPeople"),
    }


def maybe(d: dict, key, f):
    if key in d:
        return f(d[key])
    return None
