import csv

from fca.api_models import Context


def get_context(
    file_test="spect/spect_heart/SPECT.test", file_train="spect/spect_heart/SPECT.train"
):
    I = []
    with open(file_test, "r") as f:
        csv_file_test = csv.reader(f)
        for row in csv_file_test:
            attrs = [x == "1" for x in row]
            I.append(attrs)

    with open(file_train, "r") as f:
        csv_file_test = csv.reader(f)
        for row in csv_file_test:
            attrs = [x == "1" for x in row]
            I.append(attrs)

    A = [str(x + 1) for x in range(23)]
    O = [str(x + 1) for x in range(len(I))]
    return Context(O, A, I)
